﻿using Verse;

namespace ItsTimeToStop
{
	/// <summary> Functions to check compatibility with other mods </summary>
	internal static class ModCompatib
	{
		/// <summary> Return TRUE if a soft-incompatible mod is loaded. These mods can
		/// work with mine, and there are no crashes, that's why I don't mark them
		/// "incompatible" in the xml config file. But they cause minor problems,
		/// which the user should be aware of </summary>
		public static bool IsAnySoftIncompatibleModLoaded()
		{
			return IsNoPauseChallengeLoaded(); // || isOtherModLoaded...;
		}

		public static bool IsNoPauseChallengeLoaded()
		{
			// x is an info object with version, workshop descriptions, ...
			var x = ModLister.GetActiveModWithIdentifier("brrainz.nopausechallenge");
			return x != null;
		}
	}
}
