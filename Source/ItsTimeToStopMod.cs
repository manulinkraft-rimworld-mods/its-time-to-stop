﻿using LudeonTK;
using System;
using UnityEngine;
using Verse;

namespace ItsTimeToStop
{
	/// <summary> main class of this mod </summary>
	class ItsTimeToStopMod : Mod
	{
		public static MyModSettings UserSettings { get; private set; }

		public ItsTimeToStopMod(ModContentPack content) : base(content)
		{
			UserSettings = GetSettings<MyModSettings>();
		}

		/// <summary> Returns the translated name
		/// for the mod settings menu </summary>
		public override string SettingsCategory()
			=> "SettingsCategoryName".Translate();

		/// <summary> The max value for the time limit that can be chosen in the
		/// settings. The user can edit it in the "tweakvalues menu" in dev mode
		/// </summary>
		[TweakValue("max allowed play time setting", 5, 2 * 60)]
		public static float MaxAllowedPlayTime = 60;

		/// <summary> draws inside <paramref name="inRect"/>, A Unity rectangle
		/// with the size of the popup window of my app settings </summary>
		public override void DoSettingsWindowContents(Rect inRect)
		{
			var ls = new Listing_Standard();
			ls.Begin(inRect);

			ls.Label("SettingsNoteOnRestarting".Translate());
			ls.Gap(gapHeight: 12);

			ls.Label("SettingsLabelMinutes"
				.Translate(UserSettings.AllowedMinutes));

			// allow the user to choose from 1 minute up to 60 minutes
			UserSettings.AllowedMinutes = ls.Slider(
				UserSettings.AllowedMinutes, 1, MaxAllowedPlayTime);

			ls.Gap(gapHeight: 36);

			// the user can write a message
			// that will be displayed once every 20 seconds
			ls.Label("SettingsLabel2".Translate());
			UserSettings.RepeatedMessage = ls.TextEntry(
				UserSettings.RepeatedMessage);

			ls.Gap(gapHeight: 36);

			ls.CheckboxLabeled("SettingsLabel3".Translate(),
				ref UserSettings.ShowAfterReboot);

			ls.Gap(gapHeight: 36);

			// convert 1439 minutes to "23:59" for the label's readability
			string min = new DateTime(
				new TimeSpan(0, UserSettings.AllowedDayPeriod.min, 0).Ticks)
				.ToString("HH:mm");
			string max = new DateTime(
				new TimeSpan(0, UserSettings.AllowedDayPeriod.max, 0).Ticks)
				.ToString("HH:mm");

			ls.Label("SettingsLabel4".Translate(min, max));
			ls.IntRange(ref UserSettings.AllowedDayPeriod, 0, 24 * 60 - 1);

			ls.Gap(gapHeight: 36);

			ls.CheckboxLabeled("SettingsLabel5".Translate(),
				ref UserSettings.ForceQuitIn5Min);

			ls.Gap(gapHeight: 36);

			if (ModCompatib.IsAnySoftIncompatibleModLoaded())
			{
				// show a warning about soft-incompatible mods
				ls.Label("SettingsLabel6".Translate()); // title
				ls.Gap(gapHeight: 18);

				if (ModCompatib.IsNoPauseChallengeLoaded())
				{
					ls.Label("SettingsLabel7".Translate());
				}
			}

			ls.End();
			base.DoSettingsWindowContents(inRect);
		}
	}
}
