﻿using System;
using Verse;

namespace ItsTimeToStop
{
	/// <summary> Model class for the settings of this mod </summary>
	public class MyModSettings : ModSettings
	{
		/// <summary> Show the alert after this many minutes </summary>
		public float AllowedMinutes = 5;

		/// <summary> This message is flashed once every ~20 seconds </summary>
		public string RepeatedMessage = "!";

		/// <summary> In the settings, dates must be saved as strings </summary>
		private string lastShutdown;

		/// <summary> The last time the game was closed </summary>
		public DateTime? LastShutdownMoment
		{
			get => DateTime.TryParse(lastShutdown, out DateTime x) ? x : (DateTime?)null;
			set => lastShutdown = value.ToString();
		}

		/// <summary> TRUE if the alert should be shown after the user reboots 
		/// the game to circumvent the time limit </summary>
		public bool ShowAfterReboot = false;

		/// <summary> Time interval in which you are allowed to play. If you load
		/// a save file before or after this period, the alert will show.
		/// The min and max values represent the number of minutes passed from
		/// midnight. Therefore, in a day the biggest interval can be from 0 
		/// minutes (0:00 A.M.) to 1439 minutes (11:59 P.M.). For example,
		/// the user can shorten this interval to only play the game from
		/// 8:00 A.M. up to 11:30 A.M. </summary>
		public IntRange AllowedDayPeriod = new IntRange(0, 24 * 60 - 1);

		/// <summary> If the game should auto-save and shut itself down
		/// 5 minutes after the timeout altert is shown </summary>
		public bool ForceQuitIn5Min = false;

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values
				.Look(ref this.AllowedMinutes, nameof(AllowedMinutes), 5);
			Scribe_Values
				.Look(ref this.RepeatedMessage, nameof(RepeatedMessage), "!");
			Scribe_Values
				.Look(ref this.lastShutdown, nameof(lastShutdown), null);
			Scribe_Values
				.Look(ref this.ShowAfterReboot, nameof(ShowAfterReboot), false);
			Scribe_Values
				.Look(ref this.AllowedDayPeriod, nameof(AllowedDayPeriod),
				new IntRange(0, 24 * 60 - 1));
			Scribe_Values
				.Look(ref this.ForceQuitIn5Min, nameof(ForceQuitIn5Min), false);
		}
	}
}
