﻿using RimWorld;
using System.Reflection;
using Verse;

namespace ItsTimeToStop
{
	/// <summary> Contains functions to save the game </summary>
	public static class SaveUtility
	{
		/// <summary> Auto-saves the game, returning TRUE if it worked </summary>
		private static bool ForceAutosave()
		{
			FieldInfo ticksSinceLastAutoSave = typeof(Autosaver)
				.GetField("ticksSinceSave",
				BindingFlags.NonPublic | BindingFlags.Instance);

			if (ticksSinceLastAutoSave == null) return false;

			Current.Game.autosaver.DoAutosave();

			// MUST be manually set in code
			ticksSinceLastAutoSave.SetValue(Current.Game.autosaver, 0);
			return true;
		}

		/// <summary> Try auto-saving, then quitting the game. If it fails,
		/// or if the user is in dev mode, it simply shows a message </summary>
		public static void ForceSaveAndQuit()
		{
			bool Ok = ForceAutosave();
			if (Ok)
			{
				if (Prefs.DevMode)
				{
					// if we are in dev mode, don't quit
					Messages.Message("MsgDevMode".Translate(),
						MessageTypeDefOf.NeutralEvent);
				}
				else
				{
					var msg = "MsgAutosaveAndQuit".Translate();
					Messages.Message(msg, MessageTypeDefOf.NeutralEvent);
					Log.Message(msg);

					// force quit the game
					Root.Shutdown();
				}
			}
			else
			{
				Log.Warning("MsgCouldNotAutosave".Translate());
			}
		}
	}
}
