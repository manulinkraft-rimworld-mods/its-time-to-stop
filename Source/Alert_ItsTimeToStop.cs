using RimWorld;
using System;
using UnityEngine;
using Verse;

namespace ItsTimeToStop
{
	/// <summary> Show a red alert like the one for Hypothermia, 
	/// inviting the user to quit the game. It can NOT be dismissed </summary>
	public class Alert_ItsTimeToStop : Alert_Critical
	{
		/// <summary> We read it from the settings only once, at startup
		/// => to raise the treshold, you have to restart the game </summary>
		private static readonly float AllowedSeconds
			= ItsTimeToStopMod.UserSettings.AllowedMinutes * 60;

		/// <summary> TRUE if the alert should be shown right after the save is loaded.
		/// If set to TRUE, it should remain TRUE until we quit the game </summary>
		private readonly bool shouldShowImmediately = false;

		/// <summary> Called every time a new savefile is loaded </summary>
		public Alert_ItsTimeToStop()
		{
			this.defaultLabel = "AlertTitleTimeout".Translate();

			if (!IsInAllowedInterval())
			{
				// it's either too soon or too late to play. show the alert.
				shouldShowImmediately = true;
			}

			// only show the alert if the user enabled the feature
			if (!ItsTimeToStopMod.UserSettings.ShowAfterReboot) return;

			// time elapsed since the last time my alert was displayed
			var elapsed = DateTime.Now -
				ItsTimeToStopMod.UserSettings.LastShutdownMoment;

			// it was not yet initialized by my alert, or invalid somehow
			if (elapsed == null || elapsed.Value.TotalMinutes < 0) return;

			// less than ... minutes passed since last shutdown
			if (elapsed.Value.TotalMinutes < CooldownMinutes)
			{
				shouldShowImmediately = true;
				this.defaultLabel = "AlertTitleEarlyReboot".Translate();
			}

		}

		/// <summary> Minimum amount of minutes that should pass before the 
		/// player is allowed to play the game again </summary>
		private const double CooldownMinutes = 10;

		/// <summary> Returns the detailed explanation for the popup </summary>
		public override TaggedString GetExplanation()
		{
			// show the alert when the game begins, because...
			if (shouldShowImmediately)
			{
				if (!IsInAllowedInterval())
				{
					// ...the user is playing either too soon or too late in the day
					return "AlertDescOutsideDailyInterval".Translate();
				}
				else
				{
					// ...the user did not wait 10 minutes before restarting the game
					return "AlertDescEarlyReboot".Translate(CooldownMinutes);
				}
			}

			// the alert is being shown because, after some minutes, a game-ending
			// condition is verified. Either...
			if (!IsInAllowedInterval())
			{
				// ... we just went outside of the allowed play time period
				return "AlertDescOutsideDailyInterval".Translate();
			}
			else
			{
				// ... or the timeout was reached
				int minutesPassed = (int)(RealTime.LastRealTime / 60);

				// if the user chose to force-quit the game 5 minutes after seeing
				// the alert, say so with a special alert description
				return ItsTimeToStopMod.UserSettings.ForceQuitIn5Min
					? "AlertDescForceQuit".Translate(minutesPassed)
					: "AlertDescTimeout".Translate(minutesPassed);
			}
		}

		/// <summary> Returns TRUE if the allowed treshold has been surpassed. See 
		/// Unity's documentation of <see cref="Time.realtimeSinceStartup"/> </summary>
		private bool EnoughSecondsPassed() => RealTime.LastRealTime > AllowedSeconds;

		/// <summary> Makes a local copy of the allowed daily playtime period, 
		/// so that the user can't just modify it with the slider once the
		/// allowed time is over </summary>
		private IntRange mAllowedDayPeriod = ItsTimeToStopMod.UserSettings.AllowedDayPeriod;

		/// <summary> The mod settings allow to specify a daily time interval
		/// in which the user is allowed to play the game. This function returns
		/// TRUE if now we are within that time interval </summary>
		private bool IsInAllowedInterval()
		{
			// how many minutes have passed since 0:00 A.M.
			int minutesSinceMidnight = (int)(DateTime.Now - DateTime.Today).TotalMinutes;

			// Log.Message("[ItsTimeToStop] Since midnight, "
			// + minutesSinceMidnight + " minutes passed");

			return minutesSinceMidnight >= mAllowedDayPeriod.min
				&& minutesSinceMidnight <= mAllowedDayPeriod.max;
		}

		/// <summary> local memory for elapsed time after the treshold </summary>
		private float timeCounter = 0;

		/// <summary> Decides wether to show the alert or not </summary>
		/// <remarks> Gets automatically throttled by the RocketMan mod </remarks>
		public override AlertReport GetReport()
		{
			if (!shouldShowImmediately
				&& !EnoughSecondsPassed()
				&& IsInAllowedInterval())
				return AlertReport.Inactive; // it's not time yet => don't show it

			// only allow "normal" speed after the time is over
			Find.TickManager.slower.SignalForceNormalSpeed();

			// Do this every 20 seconds. Due to RocketMan's throttling,
			// the counting will not be precise, but that's OK
			if (RealTime.LastRealTime - timeCounter > 20)
			{
				// the alert is being shown
				// => the user is about to shut down the game
				ItsTimeToStopMod.UserSettings.LastShutdownMoment = DateTime.Now;

				// save the shutdown time moment
				ItsTimeToStopMod.UserSettings.Write();

				// more than 20 seconds since the last message was shown
				// => send a new one.
				string msg = "MsgRepeated".Translate(
					ItsTimeToStopMod.UserSettings.RepeatedMessage);
				Messages.Message(msg, MessageTypeDefOf.NegativeEvent);

				// reset => we will wait another 20 seconds
				timeCounter = RealTime.LastRealTime;

				// pause the game. If the "No pause challenge" mod
				// is installed, this will do nothing.
				Find.TickManager.Pause();
			}

			if (RealTime.LastRealTime > AllowedSeconds + 5 * 60)
			{
				// we are now 5 minutes over the treshold: it's time
				// to auto-save and force-quit the game
				Messages.Message(
					"MsgForceQuitting".Translate(),
					MessageTypeDefOf.NeutralEvent);
				SaveUtility.ForceSaveAndQuit();
			}

			return AlertReport.Active;
		}

		/// <summary> React to the user clicking the notification </summary>
		protected override void OnClick()
		{
			switch (Event.current.button)
			{
				case 0:
					// left mouse button
					SaveUtility.ForceSaveAndQuit();
					break;
				case 1:
					// right mouse button
					break;
				default:
					// neither button was pressed => ignore
					base.OnClick();
					break;
			}
		}
	}
}
