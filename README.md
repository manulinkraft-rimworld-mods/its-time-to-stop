# It's time to STOP

a rimworld mod for time management, as seen on the [steam workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2801631657). 

All details you want to know are on the steam workshop page: usage, tips, version support, ...

**development is slow**: I have other priorities, but I still want to keep this working. Feature request may get ignored for many months, but I will try to fix game-breaking bugs if they come up.

### Installation

This repository contains both the source code and the compiled mod. To install it:
* from this website
    1. click on the button with the arrow on the left of `Clone`
    2. choose "zip" and download it
    3. open it and copy the folder `its-time-to-stop-main` to your rimworld mods folder
* or from the command line, assuming you have `git`
    1. go into your rimworld mods folder
    2. open a terminal
    3. run `git clone https://gitlab.com/manulinkraft-rimworld-mods/its-time-to-stop.git`
    4. in this case, updating is simple:
        1. go into the mod's directory
        2. double-click `auto-update.bat`

### Contributing

There is no roadmap set in stone, we can discuss it in the [issues](https://gitlab.com/manulinkraft-rimworld-mods/its-time-to-stop/-/issues).
I accept to contributions: open an issue and tell me your ideas.
Or better, open a pull request (merge request?) and I'll include your contribution if it's good.

Code style: try to emulate what you see. I don't have a rigid code style.

### License

I don't care, you're free to do whatever you want with this, as you can see in [the license](LICENSE).
I would appreciate getting mentioned/thanked when you use all or part of my project, though.

### Developer notes

Development focuses only on the latest Rimworld game version. So for example when 1.5 is released, the compiled mod for 1.4 is kept as is, and all new updates only touch the .dll files in /1.5/Assembly/, effectively "freezing" the mod for older game versions
